import React, { useEffect } from 'react';
import { Row, Col } from 'antd';
import styles from './styled.module.scss';
import ReactGA from 'react-ga4';

const GhostLegalPage = () => {
  useEffect(() => {
    ReactGA.send('pageview');
  }, []);
  return (
    <>
      {' '}
      <Row className={styles.rowContainer}>
        <Col xs={20} xl={16} className={styles.container}>
          <p>
            {' '}
            CHEK is designed to never collect or store any sensitive
            information.
          </p>{' '}
          <p>
            CHEK messages and calls cannot be accessed by us or other third
            parties because they are always end-to-end encrypted, private, and
            secure.
          </p>
          <p>
            We want you to know your rights and obligations when using CHEK
            applications (the Apps) and CHEK real-time secure communication
            services (collectively, the Services).
            <br /> Please read and understand our Terms of Service as they are a
            legal, binding agreement between CHEK and you. By using our
            Services, you confirm your acceptance of our Terms of Service. If
            you do not agree to our Terms of Service, you may not use our
            Services.
          </p>
          <h1>Terms of Service </h1>
          <h3>Updated: Oct 25th, 2022</h3>
          <p>
            CHEK Agency Limited (“CHEK”) utilizes state-of-the-art security and
            end-to-end encryption to provide private messaging, Internet
            calling, and other services to users worldwide. You agree to our
            Terms of Service (Terms) when installing CHEK applications (the
            Apps) and using other CHEK real-time secure communication services
            (collectively, the Services).
          </p>
          <p>
            If you are accepting these Terms and using the Services on behalf of
            a company, organization, government, or other legal entity, you
            represent and warrant that you are authorized to do so and have the
            authority to bind such entity to these Terms, in which case the
            words “you” and “your” as used in these Terms shall refer to such
            entity.
          </p>
          <p>
            No waiver of the Terms of Service contained herein, or amendment to
            this Agreement shall be binding on CHEK unless set out in writing,
            explicitly stating that it is such a waiver or amendment, and duly
            signed by an Officer of CHEK.
          </p>
          <p>
            These Terms of Service apply automatically to all CHEK clients upon
            subscription to the Services including subscription through a third
            party or any commercial partner.
          </p>
          <p>
            If you are a reseller of our Services, these Terms of Services
            contain provisions that you are required to apply to your customers
            (“End Users”). It is your responsibility to ensure that these
            provisions, or others no less protective of our interests, are
            included in the contracts and other agreements between you and your
            End Users. You are responsible for your End Users under these Terms
            of Service. If you intend to resell the Services, you must sign up
            through the Vendor Portal and sign our Vendor Agreement.
          </p>
          <h2>Rights of Use</h2>
          <h3>License</h3>
          <p>
            CHEK hereby grants you a temporary, limited, non-exclusive,
            personal, non-transferable, non-sub-licensable and revocable right
            and license to access and use the Services for the Permitted
            Purposes through your Account on one or more of your personal or
            business devices and for no other purposes.
          </p>
          <p>
            You expressly agree that all intellectual property rights, titles,
            and interests in and to the Services, whether registered or not, are
            and will remain at all times the sole and exclusive property of
            CHEK. Accordingly, and except as expressly provided in these Terms
            of Service, no right or license, whether express or implied, by
            estoppel, or otherwise is granted to you with respect to the
            Services or in or under any patent, trademark, copyright, or other
            intellectual property or proprietary right of CHEK or any third
            party.
          </p>
          <p>
            Any use of any of the trademarks, service marks, logos or other form
            of intellectual property appearing in connection with the Services
            without the express written consent of CHEK or the owner of the
            mark, as applicable, is strictly prohibited.
          </p>
          <h2>CHEK’s Rights </h2>
          <p>
            CHEK owns all copyrights, trademarks, domains, logos, trade dress,
            trade secrets, patents, and other intellectual property rights
            associated with our Services. You may not use our copyrights,
            trademarks, domains, logos, trade dress, patents, and other
            intellectual property rights unless you have our written permission.
            To report copyright, trademark, or other intellectual property
            infringement, please contact legal@goCHEK.org.
          </p>
          <h3>Suspension and Termination of Services</h3>
          <p>
            CHEK reserves the right to suspend or terminate at its sole
            discretion your Account or right to use the Services at any time,
            without notice to you, for any or no reason (including, without
            limitation, in the event that CHEK becomes aware or believes that
            you have breached these Terms of Service or the Privacy Policy
            (each, a Prohibited Conduct)). Any fees paid by you in advance with
            respect to any fee-based feature may not be reimbursed to you in the
            event that CHEK suspends or terminates your Account or right to use
            the Services due to any Prohibited Conduct.
          </p>
          <p>
            If your Account or right to use the Services is suspended or
            otherwise terminated, you hereby agree that CHEK will have no
            liability or responsibility of any kind or any nature whatsoever to
            you, except as provided for herein and to the fullest extent
            permitted under applicable laws. You expressly agree that you will
            not make any claim against CHEK as a result of any such change,
            modification, suspension or termination of your Account, including,
            without limitation, with respect to any lost revenue, profits or
            opportunities or on account of any expenditures made or actions
            taken in reliance on the expected continuation of the Services or
            these Terms of Service.
          </p>
          <p>
            These rights to suspend, interrupt or terminate the Services are in
            addition and without prejudice to other remedies CHEK may have at
            law or in equity. These Terms of Use will survive the suspension or
            termination of your right to access the Services.
          </p>
          <h2>About our Services</h2>
          <h3>Minimum Age </h3>
          <p>
            Anyone who is not barred from using the Services under the laws of
            the applicable jurisdiction, and is at least 16 years of age can use
            the Service. The minimum age to use our Services without parental
            approval may be higher in your home country.
          </p>
          <h3>Account</h3>
          <p>
            You will need to create an Account to access and use our Services.
            However, you will not be required to provide personal information to
            create an Account. In setting up your Account, you will be assigned
            a randomly generated (or you can set your own) CHEK Identification
            Number (GID).{' '}
          </p>
          <p>
            Your Account is for your personal use only. You acknowledge that you
            are the sole authorized user of your Account and that you are not
            authorized to use the account, username or password of anyone else
            at any time.{' '}
          </p>
          <p>
            Furthermore, you are responsible for the confidentiality of your
            Account and any personal username and/or password required by you to
            access the Services. You cannot share your password, let anyone else
            access your Account and/or the Services or do anything that might
            jeopardize the security of your Account.{' '}
          </p>
          <p>
            As your Account is personal to you, you agree that you are fully
            responsible for all activities that occur when conducted under your
            Account. If you suspect that any unauthorized party may be using
            your Account, or you suspect any other breach of security, you must
            notify CHEK immediately at legal@GoCHEK.org.{' '}
          </p>
          <p>
            We will not be liable for any damages or losses that you incur as a
            result of someone else using your Account, either with or without
            your knowledge.
          </p>
          <p>
            You can cease to use the Services and terminate your Account at any
            time.
          </p>
          <p>
            If you cancel your Account prior to the conclusion of your current
            subscription period with respect to any fee-based feature, you will
            remain responsible for payment for all fees through the conclusion
            of your current subscription period. These Terms of Service will
            survive the voluntary termination of your Account. Please note that
            it will not be possible to reactivate your GID after termination of
            your Account.
          </p>
          <h2>CHEK Website</h2>
          <p>
            The purpose of the CHEK Website is primarily to provide you with
            information about the Services, purchasing the Services, and links
            to download the Apps.
          </p>
          <h2>CHEK Messenger</h2>
          <p>
            Encrypted messages, file transfers, calls, and video conferences,
            all integrated into a super app that secures and encrypts all your
            communication. State-of-the-art technology ensures that every
            interaction with other users is protected. No one, including us, can
            listen to your calls, read your messages or compromise your file
            transfers.
          </p>
          <h2>CHEK OS</h2>
          <p>
            CHEK OS is fully locked down with full disk encryption and network
            data management, preventing malicious attacks from even the most
            sophisticated attackers.
          </p>
          <p>
            Our CHEK Messenger, when used on a device operating CHEK OS will
            provide the user with the most complete mobile security solution in
            the world.
          </p>
          <h2>Privacy of User Data </h2>
          <p>
            CHEK does not sell, rent or monetize your personal data in any way.
          </p>
          <p>
            Please read our Privacy Policy to understand how we safeguard the
            information you provide when using our Services. For the purpose of
            operating our Services, you agree to our data practices as described
            in our Privacy Policy.
          </p>
          <h2>Data Backup </h2>
          <p>
            We maintain our own backups in the event of a disaster; however, we
            do not store any data beyond contact lists and required key server
            information for Services. If you lose access to your device with the
            required private key, and do not have access to your mnemonic
            recovery phrase your account will be illegible and irretrievable.
            Your data is fully encrypted and only decryptable by you therefore
            we make no promises with regards to backups, nor guarantee your
            content in any way.
          </p>
          <h2>Data Deletion </h2>
          <p>
            Any data deletion or removal feature offered by CHEK through the App
            or otherwise (Data Deletion) will generally be available in the
            following circumstances:
          </p>
          <ol>
            <li>
              You wish to delete your communications or other data within the
              App
            </li>
            <li>
              You have performed the maximum failed attempts (2-10) to enter
              your password to sign into your Account, as set in your App
              settings configuration
            </li>
            <li>You delete your Account in the App settings</li>
            <li>You Remote Wipe your Account</li>
            <li>Your Account expires or is terminated</li>
          </ol>
          <p>
            We recommend that you backup the information stored on your account
            on a regular basis, or prior to using any Data Deletion feature
            (otherwise than through any CHEK back-up feature). When your Account
            expires or is terminated, the CHEK data contained on your account
            will generally be irrevocably erased.
          </p>
          <p>
            CHEK assumes no responsibility for the use of any Data Deletion
            feature and is not responsible for any loss of data on your account.
            In particular, the CHEK Parties will in no event be liable for the
            loss of any data on your or their accounts resulting from use of the
            use of Data Deletion features. CHEK will not be responsible for any
            delays in performing data deletion under the “Remote Wipe” feature
            arising out of any circumstances, nor will CHEK be responsible if
            the data is not deleted in its entirety. This exclusion of liability
            will apply to the fullest extent permitted by law.
          </p>
          <h2>Updates and Maintenance</h2>
          <p>
            CHEK strives to ensure that our Services are continuously available
            for use. However, there may be occasions when they are interrupted,
            including for maintenance, upgrades, or to resolve network or
            equipment failures. We may discontinue some or all of our Services,
            including certain features and the support for certain devices and
            platforms.
          </p>
          <p>
            CHEK may review, update, upgrade or modify the Services, including
            any features therein (collectively, the Updates), at any time, in
            its sole discretion, and without notice to you. Any Update will also
            be governed by and subject to these Terms of Service. Should any
            Update be accompanied by a separate license or subject to other
            terms of service, then such other license and terms of service will
            also govern the Services. You understand and agree that temporary
            interruptions of the Services may occur as normal events and that
            CHEK assumes no responsibility for any delays or disruption of the
            Services.
          </p>
          <h2>Subscription</h2>
          <p>
            We may, at any time and at our sole discretion, change the
            subscription cost for the Services and/or add any fee-based
            features. We may, at our sole discretion, add, remove or change the
            Services or any features we offer or the fees (including the amount
            and type of fees) we charge at any time. If we introduce a new
            fee-based feature or charge a new fee, we will establish and notify
            you of the fees for that service at the launch of the service or
            when we start charging the new fee. If we notify you of new fees or
            changes to fees for an existing service, then you agree to pay all
            fees and charges specified and all applicable taxes for your
            continued use of the applicable service.
          </p>
          <h2>Third Parties</h2>
          <p>
            We work with third parties to provide some of our Services. For
            example: we use Apple and Android app stores to host the Apps for
            download/purchase; we use Stripe to charge purchases on our Website.
            These providers are bound by their Privacy Policies to safeguard
            that information.
          </p>
          <h2>Limitation of Emergency Services</h2>
          <p>
            CHEK Messenger is a messaging app, and does not have the ability to
            communicate with non-CHEK apps or services. The Messenger part of
            our Service does not provide access to public emergency service
            providers like the police, fire department, hospitals, or other
            public safety organizations.
          </p>
          <h2>Legal and Acceptable Use</h2>
          <h3>Our Policies</h3>
          <p>
            You must use our Services according to our Terms of Service. If we
            disable your account for a violation of our Terms, you will not
            create another account without our permission.
          </p>
          <h3>Customers and End Users are Solely Responsible for Content</h3>
          <p>
            We do not review, edit, censor, or take responsibility for any
            information customers or End Users may create. As a result, we
            cannot, and do not, accept any responsibility from customers, end
            users, or third parties, resulting from inaccurate, unsuitable,
            offensive, or illegal content or transactions. While we do not
            review, edit, censor, or take responsibility for any customer
            created content, we reserve the right to take any and all
            appropriate action where inaccurate, unsuitable, offensive, or
            illegal content or transactions is brought to our attention. We
            respect the privacy of our customers, but will NOT be held
            responsible for housing criminal activity, even by omission, or
            failure to investigate claims.
          </p>
          <p>
            We specifically reserve the right to refuse to provide the Services
            to customers or End Users engaged in the dissemination of material
            that may cause us to be subject to attacks on our network, or that
            while technically legal, run counter to our corporate principles.
            This type of content may include, but is not limited to, racist,
            pornographic, hateful material or generally any material that
            creates or may create, in our sole judgement, customer service or
            abuse issues for us.
          </p>
          <h2>Abusive Conduct</h2>
          <p>
            You must not (or assist others to) access, use, modify, distribute,
            transfer, or exploit our Services in unauthorized manners, or in
            ways that harm CHEK, our Services, or systems.{' '}
          </p>
          <p>
            Specific activities that are abusive include, but are not limited
            to:
          </p>
          <ol>
            <li>
              Activities that violate export control laws or regulations,
              including those related to software or technical information or
              violating laws or regulations concerning the doing of business
              with certain sanctioned countries or with designated persons or
              entities.
            </li>
            <li>
              Forging, misrepresenting, omitting or deleting message headers,
              return mailing information, and/or internet protocol addresses, to
              conceal or misidentify the origin of a message;
            </li>
            <li>
              Any action which directly or indirectly results in any of our IP
              space being listed on any abuse database;
            </li>
            <li>
              Introducing whether unintentionally, knowingly or recklessly, any
              virus or other contaminating code into the Services or creating or
              sending Internet viruses, worms or Trojan horses, flood or mail
              bombs, or engaging in denial of service attacks;
            </li>
            <li>
              Collecting or using information, including email addresses, screen
              names or other identifiers, through deceit, (such as, phishing,
              Internet scamming, password robbery, spidering, and harvesting);
            </li>
            <li>
              Hacking, and/or subverting, or assisting others in subverting, the
              security or integrity of our products or systems, including
              unauthorized access to or use of data, systems or networks,
              comprising any attempt to probe, scan or test the vulnerability of
              a system or network or to breach security or authentication
              measures without express authorization of the owner of the system
              or network.
            </li>
            <li>
              Distributing advertisement delivery software unless: (i) the user
              affirmatively consents to the download and installation of such
              software based on a clear and conspicuous notice of the nature of
              the software, and (ii) the software is easily removable by use of
              standard tools for such purpose included on major operating
              systems (such as Microsoft’s “add/remove” tool);
            </li>
            <li>
              Exchanging communication pertaining to the commission of an
              illegal activity or soliciting the performance of any illegal
              activity, even if the activity itself is not performed;
            </li>
            <li>
              Acting in any manner that might subject us to unfavorable
              regulatory action, subject us to any liability for any reason, or
              adversely affect our public image, reputation or goodwill, as
              determined by us in our sole and exclusive discretion.
            </li>
            <li>
              Interference with a third party’s use of CHEK’s network or
              Services, or ability to connect to the Internet or provide
              services to Internet users.
            </li>
            <li>
              Facilitating, aiding, or encouraging any of the above activities,
              whether using CHEK’s network or Services by itself or via a third
              party’s network or service.
            </li>
          </ol>
          <h2>Non-Permitted Use</h2>
          <p>
            You are responsible for all information, content or elements that
            you upload, submit, exchange or transfer using the Services, and you
            can only submit legal content that you are allowed to upload,
            exchange or transfer via the Services.{' '}
          </p>
          <p>
            If you make any content available, you represent that you have the
            right to do so. You acknowledge that CHEK will not, under any
            circumstances, be liable in any way for any information, content or
            elements uploaded, submitted, exchanged or transferred by you or any
            other user of the Services.
          </p>
          <p>
            CHEK exists to build private and secure technology, and defend your
            right to privacy and security in the digital world.{' '}
          </p>
          <p>
            CHEK is made solely for lawful purposes. We do not condone illegal
            activities and we must expressly prohibits the following usage of
            our Services:
          </p>
          <ol>
            <li>
              Constitutes, depicts, fosters, promotes or relates in any manner
              to child pornography, bestiality, non-consensual sex acts, or
              otherwise unlawfully exploits persons under 18 years of age;
            </li>
            <li>
              That is excessively violent, incites violence, threatens violence,
              contains harassing content or hate speech, creates a risk to a
              person’s safety or health, or public safety or health, compromises
              national security or interferes with an investigation by law
              enforcement;
            </li>
            <li>
              Is unfair or deceptive under the consumer protection laws of any
              jurisdiction, including chain letters and pyramid schemes;
            </li>
            <li>Is defamatory or violates a person’s privacy;</li>
            <li>Is otherwise malicious, fraudulent, or morally repugnant.</li>
          </ol>
          <p>
            If CHEK finds out or reasonably suspects that you used or are using
            the Services for any Non-Permitted Use, CHEK reserves the right to
            suspend or terminate your Account or otherwise refuse to provide you
            access to the Services (or any part thereof), even if the order was
            placed and confirmed or the Non-Permitted Use has ceased. The right
            of CHEK to suspend or terminate your Account is without prejudice to
            any other remedy that CHEK may have or reasonable action that CHEK
            may take.
          </p>
          <h2>Disclaimer and Liabilities</h2>
          <h3>Services Provided</h3>
          <p>
            CHEK focuses profound attention on updating and upgrading its
            applications and services in order to provide best-in-class
            protection against current and developing data security risks.
          </p>
          <p>
            As such, to the fullest extent allowed under applicable laws: (a)
            the Services are provided on an “as is” and “as available” basis
            without any warranty of any kind, and (b) CHEK disclaims all express
            implied, oral or written guarantees, warranties and representations
            pertaining to: (i) the communication, (ii) the suitability of the
            services and content for a particular purpose or the fact that the
            Services and content will meet your expectations, (iii) the fact
            that they will run without delays, errors or interruptions and that
            such delays, interruptions or errors will be addressed in a timely
            fashion (or can be addressed at all), (iv) the fact that the
            Services and their content are or will remain compatible with your
            devices, (v) the absence of adverse effect on your system,
            technology or data, and (vi) the Services’ merchantability other
            than as provided for herein.
          </p>
          <p>
            Furthermore, even if CHEK makes reasonable efforts in order for the
            description of the Services and other content to be complete and
            current, the foregoing may include inaccuracies, typographical
            errors (and other errors) and omissions relating to the description
            and availability, or other information. For instance, the Services
            may display information about some features that are no longer
            available (or slightly changed).
          </p>
          <p>Likewise, some features may be deleted, upgraded or modified. </p>
          <p>
            Furthermore, CHEK does not control the information offered or made
            available on or through the Services. Accordingly, CHEK insists on
            the fact that it is not making representations or warranties that
            the information will be accurate or complete, up-to-date or
            relevant, and accordingly any decision, action taken, or failure to
            act, in reliance on any content or Services is your sole
            responsibility and liability.
          </p>
          <p>
            You understand and agree that the access or use of the Services is
            left to your entire discretion and hence, is made at your own risks.
            CHEK disclaims any responsibility regarding the use of the Services,
            and is not responsible for any damage, including damages caused by
            use or misuse of the Services, subject however to applicable laws.
          </p>
          <p>
            This warranty disclaimer extends to any oral or written information
            you may have received from CHEK, its employees, third-party Vendors,
            agents or affiliates. You may not rely on such information.
          </p>
          <p>
            Some jurisdictions do not allow CHEK to exclude certain warranties.
            If this applies to you, your warranty is limited to 90 days from the
            effective date for an individual service.
          </p>
          <h2>Limitation of Liability</h2>
          <p>
            Under no circumstances will CHEK or any other entity of the CHEK
            Agency, their respective directors, officers, employees,
            shareholders, affiliates, business partners and third-party
            contractors, suppliers and licensors (collectively, the CHEK
            Parties) be liable to you, or to any other party, for any losses,
            costs or damages of any kind or nature whatsoever that are suffered
            or incurred in any connection with the use of (or the inability to
            use) the Services, or any CHEK Content (regardless of the form of
            action or theory of liability, including for breach of contract,
            tort, negligence, equity, strict liability, by statute or otherwise
            and regardless of the occurrence of a fundamental breach or failure
            of essential purpose).
          </p>
          <p>
            In no event whatsoever will any of the CHEK Parties be liable for
            any special, exemplary, punitive, consequential, incidental or
            indirect damages of any kind or nature whatsoever that are suffered
            or incurred in connection with the provision of the Services
            (including in connection with the transmission or downloading or
            storage of any data or submissions to or from the Services or the
            use of, or reliance on, any CHEK Content or other information or
            data contained on or provided through the Services, or loss of or
            damage to files or data or theft of your data or information that
            may occur if you do not implement passcodes or your failure to
            protect your device or other access safeguards or any costs of
            recovering or reproducing any files or data or loss of use or lack
            of availability of services or any business interruption or loss of
            revenue, opportunity or profit or any other economic loss
            whatsoever) however caused and regardless of the form or cause of
            action and whether or not foreseeable, even if the CHEK Parties or
            any of them has been informed in advance or ought reasonably to have
            known of the potential for such damages.{' '}
          </p>
          <p>
            This exclusion of liability will apply to the fullest extent
            permitted by law.
          </p>
          <h2>Indemnification</h2>
          <p>
            You agree to indemnify, defend and hold harmless CHEK and its
            parent, subsidiary and affiliated companies, Third Party Service
            providers and each of their respective officers, directors,
            employees, shareholders and agents (each an “Indemnified Party” and,
            collectively, “Indemnified Parties) from and against any and all
            claims, damages, losses, liabilities, suits, actions, demands,
            proceedings (whether legal or administrative), and expenses
            (including, but not limited to, reasonable attorneys’ fees)
            threatened, asserted, or filed by a third party against any of the
            Indemnified Parties arising out of or relating to: (i) your use of
            the Services; (ii) any violation by you of any of CHEK’s policies;
            (iii) any breach of any of your representations, warranties or
            covenants contained in these TOS; (iv) any acts or omissions by you;
            (v) any material supplied by you infringing or allegedly infringing
            on the proprietary rights of a third party; and/or (vi) infringement
            related to products sold using the Services. The terms of this
            paragraph shall survive any termination of these TOS. For the
            purpose of this paragraph only, the term “you” as set out in
            sub-paragraphs (i) through (vi) include you, End Users, visitors to
            your website, and users of your products or services, the use of
            which is facilitated by us.
          </p>
          <h2>References to Services</h2>
          <p>
            You agree that you will not, directly or indirectly, orally, in
            writing or through any medium, including, but not limited to, the
            press or other media, (i) make any false, misleading or inaccurate
            statement regarding CHEK and/or the Services, (ii) suggest that you
            are affiliated, associated or partnered with or in any other form of
            business relationship with CHEK, and (iii) make any representation,
            warranty or guarantee with respect to the Services, in each case
            unless otherwise expressly authorized by CHEK.
          </p>
          <p>Modification of these Terms of Service</p>
          <p>
            CHEK reserves the right, in its sole discretion, to modify, replace
            or otherwise update these Terms of Service at any time. CHEK may
            notify you of any material change to these Terms of Use by sending
            you a notification through the App, or by other appropriate
            communication means. Furthermore, an updated version of these Terms
            of Service will be published on our website, currently located at
            https://www.GoCHEK.org/Legal, each time a change is made.
          </p>
          <p>
            You can tell if these Terms of Service have changed by checking the
            last updated date that appears at the beginning of these Terms of
            Service. CHEK strongly encourages you to review these Terms of
            Service periodically, as your continued use of the Services will
            constitute acceptance of any updated, modified or replaced Terms of
            Service. Should you disagree with any updates or amendments made to
            the Terms of Service, you must immediately stop accessing or using
            the Services.
          </p>
          <h2>Force Majeure</h2>
          <p>
            CHEK will not be liable or responsible for any failure to perform,
            or delay in performance of, any of its obligations under these Terms
            of Service or in connection with the Services that is caused by
            events outside its reasonable control, including, without
            limitation, fire, flood, pandemic, extreme climatic conditions or
            other acts of God, failure by telecommunication providers, strikes,
            picketing, lockout or other labor disputes and/or governmental acts;
            and such failure or delay will not constitute a breach of these
            Terms of Service.
          </p>
          <h2>Final</h2>
          <h3>Choice of Law, Jurisdiction and Venue</h3>
          <p>This Agreement is governed by the laws of Hong Kong.</p>
          <h3>Severability</h3>
          <p>
            In the event that any of the terms of these Terms of Service become
            or are declared to be illegal or otherwise unenforceable by any
            court of competent jurisdiction, such term(s) shall be null and void
            and shall be deemed deleted from these Terms of Service. All
            remaining terms of these Terms of Service shall remain in full force
            and effect.
          </p>
          <h3>Contact/Notices</h3>
          <p>
            If you have any questions about the Terms of Service, or need to
            provide notice to, or communicate with, CHEK under the Term of
            Service, please contact CHEK by email at legal@GoCHEK.org. CHEK may
            provide notices or communications to you through the Apps and you
            agree that such notices will constitute notice to you whether or not
            you actually access the notice.
          </p>
        </Col>
      </Row>
    </>
  );
};
export default GhostLegalPage;
