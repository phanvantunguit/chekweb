import { Row, Col } from 'antd';
import styled from 'styled-components';

const WrapperLayout = styled.div`
  margin-top: -5rem;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100.8vh;
  @media (max-width: 768px) {
    height: 130vh;
  }
`;
const WrapperContact = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  /* max-width: 1080px; */
  .ant-form {
    width: 100%;
  }
  .ant-form-item-label > label {
    color: #686868;
  }
  @media (max-width: 768px) {
    padding: 50px 0;
    .ant-form {
      width: 90%;
      margin: 0;
    }
  }
  @media (max-width: 768px) {
    padding: 50px 0;
  }
`;
const ContactContent = styled.div`
  font-style: normal;
  font-weight: 400;
  font-size: 24px;
  line-height: 36px;
  color: #686868;
  span {
    font-style: normal;
    font-weight: 700;
    font-size: 40px;
    line-height: 60px;
    display: flex;
    align-items: center;
    color: #686868;
  }
`;
const WrapperForm = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: #686868;
  .ant-input {
    background: #d9d9d9;
    border: 1px solid rgba(0, 0, 0, 0.25);
    border-radius: 5px;
    span {
      color: #000000 !important;
    }
  }
  .ant-input:hover {
    background: rgba(57, 173, 170, 0.3);
    color: #686868;
    transform: scale(1.02);
  }
  .ant-input-status-error:not(.ant-input-disabled):not(.ant-input-borderless).ant-input {
    background: rgba(57, 173, 170, 0.3);
    color: #686868;
  }
  .ant-select.ant-select-in-form-item {
    background: #d9d9d9;
    border-radius: 5px;
    height: 45px;
    span {
      color: #000000 !important;
    }
    .ant-select-selector {
      background: #d9d9d9;
      border: 1px solid rgba(0, 0, 0, 0.25);
      border-radius: 5px;
      height: 44px;
      span {
        color: #000000 !important;
      }
    }
  }

  @media (max-width: 768px) {
    flex-direction: column;
  }
`;
const WrapperFormLeft = styled.div`
  width: 100%;
  .ant-col-16 {
    max-width: 100%;
  }
  .ant-form-item-control-input-content {
    margin: 0px 20px;
  }
  @media (max-width: 1000px) {
    .ant-form-item-control-input-content {
      margin: 0px 2px;
    }
  }
`;
const WrapperFormRight = styled.div`
  width: 100%;
  margin-left: 32px;
  margin-right: -8rem;
  .ant-col-offset-8 {
    margin-left: 0;
  }
  .ant-btn-primary {
    width: 176px;
    height: 42px;
    background: linear-gradient(0deg, #39adaa, #39adaa), #39adaa;
    border-radius: 5px;
    border: none;
    &:hover {
      background: linear-gradient(
        225deg,
        rgb(24, 200, 255) 14.89%,
        rgb(45, 198, 89) 85.85%
      );
    }
  }
  @media (max-width: 768px) {
    margin: 0;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;
const WrapperRowFirst = styled(Row)`
  display: flex;
  width: 100%;
  justify-content: space-between;
`;
const WrapperCol = styled(Col)`
  display: flex;
  justify-content: center;
  align-items: center;
`;
const WrapperCapcha = styled.div`
  @media (max-width: 500px) {
    margin-left: -22px;
  }
`;
export {
  WrapperLayout,
  WrapperContact,
  ContactContent,
  WrapperForm,
  WrapperFormLeft,
  WrapperFormRight,
  WrapperRowFirst,
  WrapperCol,
  WrapperCapcha,
};
