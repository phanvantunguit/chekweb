import React, { useEffect, useState } from 'react';
import {
  Row,
  Col,
  Progress,
  Checkbox,
  Button,
  Form,
  Input,
  Select,
} from 'antd';
import styles from './styled.module.scss';
import ReactGA from 'react-ga4';
import {
  WrapperLayout,
  WrapperContact,
  ContactContent,
  WrapperForm,
  WrapperFormLeft,
  WrapperFormRight,
  WrapperRowFirst,
  WrapperCol,
  WrapperCapcha,
} from './Company.style';
import UploadHandle from './Upload';
import UploadHandleBack from './UploadBack';
import ReCAPTCHA from 'react-google-recaptcha';

const AboutCompany = () => {
  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };

  /* eslint-disable no-template-curly-in-string */
  const validateMessages = {
    required: '${label} is required!',
    types: {
      email: '${label} is not a valid email!',
      number: '${label} is not a valid number!',
    },
    number: {
      range: '${label} must be between ${min} and ${max}',
    },
  };
  useEffect(() => {
    ReactGA.send('pageview');
  }, []);
  const [percent, setPercent] = useState('50');
  const [visible, setVisible] = useState(true);
  const [enable, setEnable] = useState(true);

  const onFinish = (values) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  useEffect(() => {
    setTimeout(
      () =>
        window.scrollTo({
          top: 0,
          behavior: 'smooth',
        }),
      0
    );
  }, []);

  return (
    <>
      <Row className={styles.rowContainer}>
        <Col span={24} className={styles.container}>
          <img src="/RDLeftBg.jpg" height={'759px'} width={'auto'} />
        </Col>
        <Col span={24} className={styles.blockImage}>
          <Col span={20}>
            <div className={styles.blockLeft}>
              <h1>
                AUTHORIZED PARTNER
                <br />
                APPLICATION FORM
              </h1>
              <div className={styles.blockTitle}>
                <p>
                  Thank you for your interest in becoming
                  <br /> a CHEK’s Partner
                  <br />
                  <br /> Please read the following application
                  <br /> requirements before applying.
                </p>
                <img src="/LineHeight.png" height={'100px'} width={'5px'} />
                <hr />
              </div>
            </div>
          </Col>
          <img src="/GhostPartner/Image.jpg" width={'auto'} height={'463px'} />
        </Col>
      </Row>
      {/* Section Two */}
      <Row className={styles.rowContainerTwo}>
        <Col span={20} className={styles.progressBar}>
          <Progress
            percent={percent}
            strokeColor={{ '0%': '#108ee9', '100%': '#87d068' }}
          />
        </Col>
        {visible ? (
          <Col span={20} className={styles.colContainerTwo}>
            <div>
              <a>We Offer:</a> <br />– Access to The World’s Most Complete
              Mobile Security & Self-Managed Solution <br />
              – 24/7 Live Support <br />
              – Onboarding Assistance <br />– Centralized Admin and Billing
              Portal
              <br />– Retail and Partner pricing to be provided upon successful
              applications
              <br />
              <br />
              <a>CHEK’s Partner Terms of Serivce: </a>
              <br />
              Partners acknowledge that CHEK may only be sold in the designated
              region(s) approved by management which will be outlined in the
              Partner Agreement.
              <br /> CHEK subscriptions are sold with subscription terms of 3, 6
              or 12 months and must be pre-paid by Partners. Clients may provide
              their own unlocked and non-blacklisted compatible device and
              purchase the License only. Alternatively, clients may purchase the
              License with a device included. If a client needs to purchase a
              device, Partners source their own unlocked and non-blacklisted
              compatible devices for resale to the client. Subscription and
              device pricing must always be quoted SEPARATELY. <br />
              Rates must be charged at no less than our mandatory minimum rates.
              <br />
              Partners are required to have a minimum of 75 active clients
              within the first 12 months of becoming a Partner to continue
              selling.
              <br />
              Partners acknowledge and agree that the CHEK Marks are and will
              remain the sole and exclusive property of CHEK. <br />
              Partners acknowledge that CHEK reserves the exclusive right to
              promote and market the Services. Partners may not promote or sell
              CHEK online via their own website, social media, or marketplaces.
              The permitted uses of CHEK include the prevention of identity
              theft, hacking and malicious attacks; protection of personal
              privacy rights; and the secure operation of legitimate personal
              and business affairs. Should a Partner become aware or have
              knowledge of a potential or existing client`s intention to use
              CHEK in a manner breaching our Terms of Service, the Partner MUST
              deny the sale or terminate the service of the account in question.
              <br /> Please completely read and understand our Terms of Service
              and Privacy Policy. Partners shall not make representation,
              warranties, conditions, or guarantees concerning the Services that
              are in addition to those made by CHEK as reflected in the Terms of
              Service or other documentation provided to the Partner by CHEK or
              published on the CHEK website, nor shall the Partner take any
              action which may damage the goodwill, name, and reputation of
              CHEK.
            </div>
            <div className={styles.checkBoxCustom}>
              <Checkbox onChange={() => setEnable(!enable)}>
                I agree that I have completely read, understand and will comply
                to all of the terms mentioned above.
              </Checkbox>
            </div>
            <div>
              <Button
                className={styles.buttonCustom}
                onClick={() => {
                  setPercent('99');
                  setVisible(false);
                }}
                disabled={enable}
              >
                NEXT
              </Button>
            </div>
          </Col>
        ) : (
          <Col span={20} className={styles.colContainerTwo}>
            <WrapperContact>
              <Form
                {...layout}
                name="nest-messages"
                onFinish={onFinish}
                validateMessages={validateMessages}
              >
                <WrapperForm>
                  <WrapperFormLeft>
                    <WrapperRowFirst>
                      <Col span={12}>
                        <Form.Item
                          name={['user', 'name']}
                          rules={[{ required: true }]}
                        >
                          <label>First Name*</label>
                          <Input size="large" placeholder="e.g. John" />
                        </Form.Item>
                      </Col>
                      <Col span={12}>
                        <Form.Item
                          name={['user', 'name']}
                          rules={[{ required: true }]}
                        >
                          <label>Last Name*</label>
                          <Input size="large" placeholder="e.g. Doe" />
                        </Form.Item>
                      </Col>
                    </WrapperRowFirst>
                    <WrapperRowFirst>
                      <Col xs={24} xl={8}>
                        <Form.Item name={['user', 'email']}>
                          <label>D.O.B*</label>
                          <Input size="large" placeholder="mm/dd/yyyy" />
                        </Form.Item>
                      </Col>
                      <Col xs={24} xl={8}>
                        <Form.Item
                          name={['user', 'email']}
                          rules={[{ type: 'email' }]}
                        >
                          <label>Email*</label>
                          <Input size="large" placeholder="Input your email " />
                        </Form.Item>
                      </Col>{' '}
                      <Col xs={24} xl={8}>
                        <Form.Item
                          name={['user', 'email']}
                          rules={[{ type: 'email' }]}
                        >
                          <label>Phone*</label>
                          <Input size="large" placeholder="Input your phone " />
                        </Form.Item>
                      </Col>
                    </WrapperRowFirst>

                    <Form.Item name={['user', 'website']}>
                      <label>Document*</label>
                      <Input size="large" />
                    </Form.Item>
                    <WrapperRowFirst>
                      <WrapperCol xs={24} sm={12}>
                        <Form.Item>
                          <label>•Front Side</label>
                          <UploadHandle />
                        </Form.Item>
                      </WrapperCol>
                      <WrapperCol xs={24} sm={12}>
                        <Form.Item>
                          <label>•Back Side</label>
                          <UploadHandleBack />
                        </Form.Item>
                      </WrapperCol>
                    </WrapperRowFirst>
                    <Form.Item name={['user', 'website']}>
                      <label>Alternative Contact Method*</label>
                      <Input size="large" />
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <label>Company Name*</label>
                      <Input size="large" />
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <label>Corporate Registration Number*</label>
                      <Input size="large" />
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <label>
                        Which Country and City Is the Company Registered In?*
                      </label>
                      <Input size="large" />
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <label>What Type of Industry Is Your Company In?*</label>
                      <Input size="large" />
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <label>
                        Which Country and Cities Will Be Your Primary
                        Market(s)?*
                      </label>
                      <Input size="large" />
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <label>
                        Do You Have a Store(s) That You Will Sell From?*
                      </label>
                      <Input size="large" />
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <label>
                        Do You Currently Sell or Have You Ever Sold Secure
                        Communications Before?*
                      </label>
                      <Select
                        defaultValue="Yes"
                        options={[
                          {
                            value: 'Yes',
                            label: 'Yes',
                          },
                          {
                            value: 'No',
                            label: 'No',
                          },
                        ]}
                      />
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <label>Will You Be Working with Any Partners?*</label>
                      <Select
                        defaultValue="Yes"
                        options={[
                          {
                            value: 'Yes',
                            label: 'Yes',
                          },
                          {
                            value: 'No',
                            label: 'No',
                          },
                        ]}
                      />
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <label>Describe Your Team*</label>
                      <Input size="large" />
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <label>
                        Briefly Explain Your Mobile Security Knowledge*
                      </label>
                      <Input size="large" />
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <label>
                        Have You Successfully Built Partners Networks in the
                        Past?*
                      </label>
                      <Input size="large" />
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <label>
                        Do You Have Relations to Source Compatible Devices That
                        Are Unlocked and Non-Blacklisted?*
                      </label>
                      <Input size="large" />
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <la>
                        Upon Starting as a GHOST’s Partner, How Many Active
                        Clients Do You Estimate You Will Have Within:
                      </la>
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <label>a) Your First Month?*</label>
                      <Input size="large" />
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <label>b) Your First Six Months?*</label>
                      <Input size="large" />
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <label>c) Your First Twelve Months?*</label>
                      <Input size="large" />
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <label>
                        Have You or Your Company Ever Declared Bankruptcy?*
                      </label>
                      <Select
                        defaultValue="Yes"
                        options={[
                          {
                            value: 'Yes',
                            label: 'Yes',
                          },
                          {
                            value: 'No',
                            label: 'No',
                          },
                        ]}
                      />
                    </Form.Item>
                    <Form.Item name={['user', 'website']}>
                      <label>
                        Have You or Your Company Ever Been Convicted of a
                        Criminal Offense?*
                      </label>
                      <Select
                        defaultValue="Yes"
                        options={[
                          {
                            value: 'Yes',
                            label: 'Yes',
                          },
                          {
                            value: 'No',
                            label: 'No',
                          },
                        ]}
                      />
                    </Form.Item>

                    <Form.Item name={['user', 'introduction']}>
                      <label>
                        Please Provide a Summarized Business Plan Illustrating
                        How You Intend to Grow and Sustain Your Business:
                      </label>
                      <Input.TextArea rows={7} placeholder="Input message" />
                    </Form.Item>
                    <Form.Item>
                      <WrapperCapcha>
                        <ReCAPTCHA
                          sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
                          // onChange={onChange}
                        />
                      </WrapperCapcha>
                    </Form.Item>
                    <Form.Item>
                      <label>
                        Only complete applications will be reviewed Please Allow
                        One to Three Business Days for the Review of Your
                        Application
                      </label>
                    </Form.Item>
                  </WrapperFormLeft>
                  {/* <WrapperFormRight>
                    <ContactContent>
                      <span>Contact Us</span> <hr style={{ width: '100%' }} />{' '}
                      <p>
                        For any questions regarding business partnership do not
                        hesitate to contact us.
                      </p>
                    </ContactContent>
                    <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                      <Button type="primary" htmlType="submit">
                        Submit
                      </Button>
                    </Form.Item>
                  </WrapperFormRight> */}
                </WrapperForm>
              </Form>
            </WrapperContact>
            <div className={styles.converButton}>
              <Button
                className={styles.buttonCustom}
                onClick={() => {
                  setPercent('50');
                  setVisible(true);
                  setEnable(!enable);
                }}
              >
                BACK
              </Button>
              <Button
                className={styles.buttonCustom}
                onClick={() => setPercent('99')}
              >
                SUBMIT
              </Button>
            </div>
          </Col>
        )}
      </Row>
    </>
  );
};

export default AboutCompany;
