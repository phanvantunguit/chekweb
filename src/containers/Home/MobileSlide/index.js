import React, { useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide, useSwiper } from "swiper/react";
import styles from "./styled.module.scss";
import { Navigation } from "swiper";
// import "swiper/swiper-bundle.js";
import "swiper/swiper-bundle.min.css";

// Import Swiper styles
import "swiper/swiper.min.css";
import { left } from "styled-system";
// import "swiper/components/pagination";

// import "./slide.css";
// import required modules

export default function MobileSlide() {
  const swiperRef = useRef();
  return (
    <>
      <div className={styles.wrapperSlide}>
        <div className={styles.blockChildSlideContent}>
          <Swiper
            slidesPerView={1}
            spaceBetween={10}
            pagination={{
              clickable: true,
            }}
            modules={[Navigation]}
            onBeforeInit={(swiper) => {
              swiperRef.current = swiper;
            }}
          >
            <SwiperSlide className={styles.blockChildSlideBot}>
              <div className={styles.blockSlide}>
                <img
                  src="/GhostHome/Tab_Banking.jpg"
                  width={"100%"}
                  height={"auto"}
                />
              </div>
              <div className={styles.titleSlide}>Banking & financials</div>
              <div className={styles.content}>
                <p>
                  Information in the financial industry is of high value across
                  the board. This creates a situation where every part of it is
                  a potentially lucrative target. This threat model suggests a
                  holistic approach to cybersecurity that leaves no attack
                  surfaces uncovered. Regarding communications, protecting the
                  exchanged information is of fundamental importance for both
                  clients and financial institutions.
                </p>
              </div>
            </SwiperSlide>
            <SwiperSlide className={styles.blockChildSlideBot}>
              <div className={styles.blockSlide}>
                <img
                  src="/GhostHome/Tab_Consulting.jpg"
                  width={"100%"}
                  height={"auto"}
                />
              </div>
              <div className={styles.titleSlide}>Consulting & accountancy</div>
              <div className={styles.content}>
                <p>
                  Securing the confidentiality of high-value information is an
                  integral part of the service consulting, and accounting
                  companies offer. They access such information daily through
                  multiple channels and devices. This, in turn, creates an
                  attack vector for cybercriminals to target employee accounts
                  with phishing schemes to obtain corporate information.
                </p>
              </div>
            </SwiperSlide>
            <SwiperSlide className={styles.blockChildSlideBot}>
              <div className={styles.blockSlide}>
                <img
                  src="/GhostHome/Tab_Energy.jpg"
                  width={"100%"}
                  height={"auto"}
                />
              </div>
              <div className={styles.titleSlide}>Energy</div>
              <div className={styles.content}>
                <p>
                  High stakes mark communication in the energy industry.
                  Information regarding one company’s drilling, extraction,
                  refining, shipping, and logistic activities, can be enough to
                  affect prices in the sector instantly – causing a domino
                  effect that can influence stock markets and national
                  economies. The global scope of operations calls for
                  communicating freely, regardless of borders.
                </p>
              </div>
            </SwiperSlide>
            <SwiperSlide className={styles.blockChildSlideBot}>
              <div className={styles.blockSlide}>
                <img
                  src="/GhostHome/Tab_Legal.jpg"
                  width={"100%"}
                  height={"auto"}
                />
              </div>
              <div className={styles.titleSlide}>Legal professionals</div>
              <div className={styles.content}>
                <p>
                  Lawyers are obligated to protect the confidentiality of their
                  client's data, and law firms have to pay closer attention to
                  confidentiality than the average business. If law firms do not
                  secure their client communications and other data, they could
                  violate the attorney-client privilege, lose clients, be
                  subject to malpractice actions, damage their reputation, and
                  possibly also lose their license to practice law.
                </p>
              </div>
            </SwiperSlide>
            <SwiperSlide className={styles.blockChildSlideBot}>
              <div className={styles.blockSlide}>
                <img
                  src="/GhostHome/Tab_Pharmaceuticals.jpg"
                  width={"100%"}
                  height={"auto"}
                />
              </div>
              <div className={styles.titleSlide}>Pharmaceuticals</div>
              <div className={styles.content}>
                <p>
                  The pharmaceutical industry is one of the top cybercrime
                  targets due to the high value of companies’ intellectual
                  property and R&D data. For example, drug formulas are some of
                  the highest-value assets in the industry. At the same time,
                  enterprises in the sector are often present in multiple
                  markets, and R&D operations are spread across several
                  locations.
                </p>
              </div>
            </SwiperSlide>
            <SwiperSlide className={styles.blockChildSlideBot}>
              <div className={styles.blockSlide}>
                <img
                  src="/GhostHome/Tab_Security.jpg"
                  width={"100%"}
                  height={"auto"}
                />
              </div>
              <div className={styles.titleSlide}>Security & defense</div>
              <div className={styles.content}>
                <p>
                  When it comes to the protection of people and property, having
                  the right information at the right time is of critical
                  importance. The defense industry calls for the highest
                  available security and reliability standards and independence
                  from third-party providers.
                </p>
              </div>
            </SwiperSlide>
          </Swiper>
        </div>
        <div className={styles.onClickChangeSlide}>
          <button
            className={styles.button}
            onClick={() => swiperRef.current?.slidePrev()}
          >
            <img src="/iconback.png" width={"48px"} height={"48px"} />
          </button>
          <button
            className={styles.button}
            onClick={() => swiperRef.current?.slideNext()}
          >
            <img src="/iconnext.png" width={"48px"} height={"48px"} />
          </button>
        </div>
      </div>
    </>
  );
}
