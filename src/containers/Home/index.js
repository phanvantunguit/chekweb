import { Col, Row } from 'antd';
import React, { useEffect, useState } from 'react';
import styles from './styled.module.scss';
import LazyLoad from 'react-lazyload';
import MobileSlide from './MobileSlide/index';
import SlideDemo from './Slides/index';
import ReactGA from 'react-ga4';

const Home = () => {
  // Handle scroll run number
  const [count, setCount] = useState(0);
  useEffect(() => {
    if (offsetTop && count < 180) {
      const id = setInterval(() => setCount((item) => item + 1), 10);
      return () => {
        clearInterval(id);
      };
    }
  });

  const [offsetTop, setOffSetTop] = useState(false);
  const isTop = 450 || 100; // check position mouse
  const handleOnScroll = () => {
    if (window.pageYOffset > isTop) {
      setOffSetTop(true);
    } else {
      setOffSetTop(false);
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', handleOnScroll);
    return () => {
      window.removeEventListener('scroll', handleOnScroll);
    };
  }, [isTop, handleOnScroll]);
  // Handle scroll run number

  useEffect(() => {
    ReactGA.send('pageview');
  }, []);

  return (
    <>
      {/* <Row className={styles.rowContainer}>
        <Col span={24} className={styles.container}>
          <img
            src="/GhostHome/home_background.png"
            width={'auto'}
            height={'100%'}
          />
        </Col>
        <Col span={20} className={styles.blockImage}>
          <div className={styles.blockLeft}>
            <h1>SECURED ANONYMITY</h1>
            <a>
              TRUSTED BY TOP FIRMS <br />
              AROUND THE WORLD
            </a>
            <p>
              Built from the ground up and designed <br /> for maximum security.
            </p>
          </div>
          <div className={styles.blockRight}>
            <img
              src="/GhostHome/home_block_background.png"
              width={'548px'}
              height={'719px'}
            />
          </div>
        </Col>
      </Row> */}
      <Row className={styles.rowContainerSecond}>
        <Col span={20}>
          <LazyLoad once={true} height={'938px'} className={styles.sectiontwo}>
            <div className={styles.blockLeftSectiontwo}>
              <h1>CHEK</h1>
              {/* <h4>SECURED ANONYMITY</h4> */}
              {/* <a>
                <img src="/ButtonMore.png" />
                Learn more
              </a> */}
              <div className={styles.titleProtec}>
                {/* 360° <span>protection</span> */}
                Your Privacy is <span>Our Commitment</span>
                <p>
                  Preserving your fundamental right to privace with
                  uncompromising security.
                </p>
              </div>
            </div>
            <div className={styles.blockRightSectiontwo}>
              <div className={styles.gridBlockSctiontwo}>
                {/* <a href="#" className={styles.blockGridone}>
                  <a>CHEK STACK</a>
                  <img
                    src="/PhoneBlock1.png"
                    width={'312px'}
                    height={'142px'}
                  />{' '}
                </a> */}
                <a href="/partner" className={styles.blockGritwo}>
                  <a>
                    CHEK Partners
                    <br /> System
                  </a>{' '}
                  <div>
                    <img
                      src="/PhoneBlock3.png"
                      width={'312px'}
                      height={'272px'}
                    />
                  </div>
                </a>
                <a href="/chek-os" className={styles.blockGrithree}>
                  <a>CHEK OS</a>
                  <img
                    src="/PhoneBlock2.png"
                    width={'315px'}
                    height={'308px'}
                  />
                </a>
              </div>
            </div>
          </LazyLoad>
        </Col>
      </Row>
      {/* SectionThree */}
      <Row className={styles.rowContainerThree}>
        <Col span={20}>
          <div className={styles.blockAboutChek}>
            <h1>About CHEK</h1>
            <p>
              In the ever-growing digital world where it seems every action can
              be tracked, privacy may seem more like an ideal concept than
              reality. With an exponentially increasing number of data breaches
              and other malicious attackers looking to intercept, monitor, or
              exploit your data, your personal data and privacy are at risk
              daily.
            </p>
            <p>
              In thí day and age where more and more are working from home,
              there's little distinction between personal and professional when
              it comes to data. This means organizations must protect the
              privacy of customers and employees to protect themselves.
            </p>
            <p>
              Our mission at CHEK is to provide the tools to navigate this
              digital world world and communicate securely while preserving the
              peace of mind that your data remains yours, and yours alone.
            </p>
          </div>
          {/* <div className={styles.wrapperBlock}>
            <div className={styles.blockNumber}>
              <div>
                <h1>{count < 20 ? count : 20}K</h1>
                <p>Users around the world</p>
              </div>
              <img src="/Line1.png" height={'140px'} />
              <div>
                <h1>{count < 60 ? count : 60}+</h1>
                <p>Dedicated team members</p>
              </div>
            </div>
            <div className={styles.blockNumber}>
              <img
                src="/Line1.png"
                height={'140px'}
                className={styles.blockChild}
              />
              <div>
                <h1 className={styles.block180}>
                  <span>{count < 180 ? count : 180}</span>+
                </h1>
                <p>Countries service coverage</p>
              </div>
              <img src="/Line1.png" height={'140px'} />
              <div>
                <h1 className={styles.block99}>
                  {count < 100 ? count : 99}.9%
                </h1>
                <p>Uptime</p>
              </div>
            </div>
          </div> */}
        </Col>
      </Row>
      {/* SlideDemo */}
      <Row className={styles.rowContainerSix}>
        <Col span={20} className={styles.colContainerSix}>
          <a>
            Secure <span>your business</span> data
          </a>
          <div className={styles.secureBlockSlide}>
            <p>
              Different industries have different communication needs. As a
              result, they face different threat models – which, in turn, calls
              for different security setups. There is no one-size-fits-all
              solution to secure your business communications. Our secure
              communication solutions are designed to be flexible and scalable
              to meet the needs of different sectors.
            </p>
          </div>
          <SlideDemo />
          <MobileSlide />
        </Col>
      </Row>
      {/* SectionFour */}
      {/* <Row className={styles.rowContainerFour}>
        <Col span={20} className={styles.colContainerFour}>
          <div className={styles.inLineText}>
            Latest news <span>from Chek</span>
          </div>
          <div className={styles.blockAllSectionFour}>
            <div className={styles.blockOneInSectionFour}>
              <img src="/Ghost/ChekPhone.jpg" width={'auto'} height={'220'} />
              <a>Chek Chat 2.1.0: Streamlining the User Experience</a>
              <p>
                20th Oct 2022 · By &nbsp;<a>Chek Agency</a>{' '}
              </p>
            </div>
            <div className={styles.blockOneInSectionFour}>
              <img src="/Ghost/ChekPhone.jpg" width={'auto'} height={'220'} />
              <a>Chek OS 2.1.0: Better User experience</a>
              <p>
                20th Oct 2022 · By &nbsp;<a>Chek Agency</a>{' '}
              </p>
            </div>
            <div className={styles.blockOneInSectionFour}>
              <img src="/Ghost/ChekPhone.jpg" width={'auto'} height={'220'} />
              <a>Secure Chat 2.0.0: Enhanced Group Chat Anonymity</a>
              <p>
                20th Oct 2022 · By &nbsp;<a>Chek Agency</a>{' '}
              </p>
            </div>
          </div>
        </Col>
      </Row> */}
      {/* <Row className={styles.rowContainerFive}>
        <Col span={20} className={styles.colContainerFive}>
          <div className={styles.blockLeftFive}>
            Book a personalized demo <a>Book a demo</a>
          </div>
          <img src="/PhoneSec5.png" height={'250px'} width={'auto'} />
        </Col>
      </Row> */}
    </>
  );
};
export default Home;
