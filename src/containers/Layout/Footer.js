import React from 'react';
import {
  FooterContent,
  FooterCol,
  ConverFooter,
  BlockList,
  ListItems,
  DownloadImages,
  ConverDownload,
  CoverFooterBot,
  Privacy,
  ConverPolicy,
} from './Layout.style';

const Footer = () => {
  return (
    <FooterContent>
      <FooterCol span={20}>
        <ConverFooter>
          {/* <BlockList> */}
          {/* <ListItems>About</ListItems>
            <ListItems>
              <a>Company</a>
            </ListItems> */}
          {/* <ListItems>
              <a>Career</a>
            </ListItems>
            <ListItems>
              <a>Contact</a>
            </ListItems> */}
          {/* </BlockList> */}
          {/* <BlockList>
            <ListItems>Technology</ListItems>
            <ListItems>
              <a>Encryption</a>
            </ListItems>
            <ListItems>
              <a>GHOST Messenger</a>
            </ListItems>
            <ListItems>
              <a>CHEK 360</a>
            </ListItems>
          </BlockList> */}
          <BlockList>
            {/* <ListItems>R&D</ListItems> */}
            {/* <ListItems>Partner</ListItems> */}
            <ConverPolicy>
              <a href="/privacy-policy">Privacy Policy </a>|{' '}
              <a href="/Legal"> Terms and Conditions</a>
            </ConverPolicy>
            {/* <ConverDownload>
              <DownloadImages
                // href="https://install.goghost.org/2022"
                href="#"
                target="_blank"
              >
                <img src="/Ghost/Download_App.png" width={'100%'} />
              </DownloadImages>
              <DownloadImages
                href="#"
                // href="https://apps.apple.com/gb/app/ghost-messenger-ios/id6444491082?uo=2"
                target="_blank"
              >
                <img src="/GhostHome/LogoAppStore.png" width={'100%'} />
              </DownloadImages>
            </ConverDownload> */}
          </BlockList>
        </ConverFooter>
        <CoverFooterBot>
          <img src="./Ghost/Chek_Black.png" width={'106px'} height={'auto'} />
          <Privacy>
            <a href="/privacy-policy">Privacy Policy </a>|{' '}
            <a href="/Legal"> Terms and Conditions</a>
          </Privacy>
          <p>Copyright © 2022 Chek</p>
        </CoverFooterBot>
      </FooterCol>
    </FooterContent>
  );
};

export default Footer;
