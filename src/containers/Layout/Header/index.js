import React, { useState } from 'react';
import styles from './styled.module.scss';
import { Link } from 'react-router-dom';
import '../Slide.css';

const Header = () => {
  const [isActive, setActive] = useState(null);
  const ClickMenu = () => {
    isActive ? setActive(false) : setActive(true);
  };

  const CloseMenu = () => {
    setActive(null);
  };
  return (
    <>
      <div className={styles.container}>
        <div className={styles.blockLogo}>
          <a href="/home">
            <img src="./Ghost/Chek_Black.png" width={'150px'} height={'auto'} />
            {/* <img
              src="./GHOST.png"
              width={'80px'}
              height={'auto'}
              style={{ marginLeft: '20px' }}
            /> */}
            {/* CHEK */}
          </a>
        </div>
        <div className={styles.blockItemHeader}>
          {/* Block child */}
          <div className={styles.dropdown}>
            {/* <div className={styles.blockHeader}>
              About <img src="./dropdown.png" />
            </div> */}
            {/* <div className={styles.dropdownContent}>
              <a href="/about-company">Company</a>
              <a href="/about-career">Career</a>
              <a href="/about-contact">Contact</a>
            </div> */}
          </div>
        </div>
        {/* <div className={styles.blockItemHeader}>
          Block child
          <div className={styles.dropdown}>
            <div className={styles.blockHeader}>
              Technology{' '}
              <span className={styles.arrow180}>
                <img src="./dropdown.png" />
              </span>
            </div>
            <div className={styles.dropdownContent}>
              <Link to="/encryption">Encryption</Link>
              <a href="/ghost-stack">CHEK STACK</a>
              <a href="/ghost-os">GHOST OS</a>
            </div>
          </div>
        </div> */}
        <div className={styles.blockItemHeader}>
          {/* Block child */}
          <div className={styles.dropdown}>
            <Link to="/encryption">
              <div className={styles.blockHeader}>Encryption</div>
            </Link>
          </div>
        </div>
        <div className={styles.blockItemHeader}>
          {/* Block child */}
          <div className={styles.dropdown}>
            <Link to="/chek-os">
              <div className={styles.blockHeader}>CHEK OS</div>
            </Link>
          </div>
        </div>
        {/* <div className={styles.blockItemHeader}>
          Block child
          <div className={styles.dropdown}>
            <Link to="/a-and-d">
              <div className={styles.blockHeader}>R&D</div>
            </Link>
          </div>
        </div> */}
        <div className={styles.blockItemHeader}>
          {/* Block child */}
          <div className={styles.dropdown}>
            <Link to="/partner">
              <div className={styles.blockHeader}>Partner</div>
            </Link>
          </div>
        </div>
        <div className={styles.blockItemHeader}>
          {/* <div className={styles.dropdown}>
            <Link to="/vendor">
              <div className={styles.blockHeader}>Vendor</div>
            </Link>
          </div> */}
        </div>
        <div class="nav">
          <button
            class={
              isActive == null || !isActive ? 'btn-nav' : 'btn-nav animated'
            }
            onClick={ClickMenu}
          >
            <span class="icon-bar top"></span>
            <span class="icon-bar middle"></span>
            <span class="icon-bar bottom"></span>
          </button>
        </div>
        <div
          class={
            isActive == null
              ? 'hidden'
              : !isActive
              ? 'nav-content hideNav'
              : 'showNav nav-content'
          }
        >
          <ul class="nav-list" onClick={() => CloseMenu()}>
            <li class="nav-item">
              <Link to="/home">
                <span class="item-anchor">Home</span>
              </Link>
            </li>
            {/* <li class="nav-item">
              <Link to="/about-company">
                <span class="item-anchor">Company</span>
              </Link>
            </li>
            <li class="nav-item">
              <Link to="/about-career">
                <span class="item-anchor">Career</span>
              </Link>
            </li>
            <li class="nav-item">
              <Link to="/about-contact">
                <span class="item-anchor">Contact</span>
              </Link>
            </li> */}
            <li class="nav-item">
              <Link to="/encryption">
                <span class="item-anchor">Encryption</span>
              </Link>
            </li>
            {/* <li class="nav-item">
              <Link to="/ghost-mess">
                <span class="item-anchor">CHEK STACK</span>
              </Link>
            </li>{' '} */}
            <li class="nav-item">
              <Link to="/chek-os">
                <span class="item-anchor">CHEK OS</span>
              </Link>
            </li>{' '}
            <li class="nav-item">
              <Link to="/partner">
                <span class="item-anchor">Partner</span>
              </Link>
            </li>
            <li class="nav-item">
              <Link to="/vendor">
                <span class="item-anchor">VENDOR</span>
              </Link>
            </li>
          </ul>
          <div class="line-betwen"></div>
        </div>
      </div>
    </>
  );
};
export default Header;
