import React from "react";
import GhostPolicy from "../containers/GhostPolicy";

const GhostPolicyPage = () => {
  return <GhostPolicy />;
};

export default GhostPolicyPage;
